
# Entrevista Tecnica Manoel Junior

## Requisitos

- Python 3+
- Django

## Routes

- localhost:8000/interview/question1/
- localhost:8000/interview/question2/
- localhost:8000/interview/question3/

## Executando o codigo

- Abra cmd na pasta 'manoeljuniorinterview'
- Execute o codigo: 'python manage.py runserver'
- Abra o navegador na rota correspondente a questao e insira o conteudo do arquivo JSON como parametro
  

  Ex: localhost:8000/interview/question1/{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ]
}