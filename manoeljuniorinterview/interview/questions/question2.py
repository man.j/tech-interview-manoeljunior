from interview.questions import question1

def fees(obj):
	without_fees = question1.carts(obj)
	for x in without_fees['carts']:
		x['total'] = apply_fees(obj,x)
	with_fees = without_fees
	return with_fees

def apply_fees(obj,cart):
	for y in obj['delivery_fees']:
		min_price = y['eligible_transaction_volume']['min_price']
		max_price = y['eligible_transaction_volume']['max_price']
		if type(max_price)!=int:
			if min_price <= cart['total']:
				return cart['total'] + y['price']
		else:
			if min_price <= cart['total'] < max_price:
				return cart['total'] + y['price']