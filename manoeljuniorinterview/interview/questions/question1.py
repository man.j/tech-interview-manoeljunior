def client_sum(iterator,obj):
	s = 0
	for x in iterator['items']:
		s += item_price(x,obj['articles'])
	return s


def item_price(item,articles):
	for x in articles:
		if item['article_id']==x['id']:
			return x['price']*item['quantity']

def carts(obj):
	ret = {}
	carts = []
	for x in obj['carts']:
		cart = {}
		cart['id'] = x['id']
		cart['total'] = client_sum(x,obj)
		carts.append(cart)
	ret['carts'] = carts
	return ret