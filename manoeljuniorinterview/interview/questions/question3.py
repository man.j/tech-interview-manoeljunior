from interview.questions import question1,question2
import math

def discounts(obj):
	question1.client_sum = apply_discounts_client
	return question2.fees(obj)

def apply_discounts_client(iterator,obj):
	total = 0
	for x in iterator['items']:
		i = 0
		i = question1.item_price(x,obj['articles'])
		if 'discounts' in obj:
			for y in obj['discounts']:
				if x['article_id'] == y['article_id']:
					if y['type']=='amount':
						i -= y['value'] * x['quantity']
					elif y['type']=='percentage':
						i -= math.ceil((y['value'] * i) / 100)
		total += i
	return total