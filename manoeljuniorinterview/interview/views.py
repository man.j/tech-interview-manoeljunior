import json
from django.shortcuts import HttpResponse
from interview.questions import question1,question2,question3


# Create your views here.
def q1(request, json_data):
    return HttpResponse(json.dumps(question1.carts(json.loads(json_data))), content_type='application/json')

def q2(request, json_data):
    return HttpResponse(json.dumps(question2.fees(json.loads(json_data))), content_type='application/json')

def q3(request, json_data):
    return HttpResponse(json.dumps(question3.discounts(json.loads(json_data))), content_type='application/json')