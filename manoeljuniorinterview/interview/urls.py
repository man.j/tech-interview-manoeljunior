from django.urls import path

from . import views

urlpatterns = [
    path('question1/<str:json_data>', views.q1, name='q1'),
    path('question2/<str:json_data>', views.q2, name='q2'),
    path('question3/<str:json_data>', views.q3, name='q3'),
]